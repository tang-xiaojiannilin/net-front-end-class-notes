import fs from 'fs';

let arr = []; // 定义一个空数组用来存储结果

function listArr(filePath) {
    let files = fs.readdirSync(filePath); // 读取指定路径下的所有文件和目录

    files.forEach(file => {
        let fullPath = filePath + '/' + file; // 构建文件的完整路径

        // 检查文件的类型
        let stats = fs.statSync(fullPath);
        if (stats.isDirectory()) {
            listArr(fullPath); // 如果是目录，则递归调用listArr处理目录中的内容
        } else {
            arr.push(fullPath); // 如果是文件，则将路径添加到结果数组中
        }
    });
}

listArr('./'); // 调用函数并传入指定路径

console.log(arr); // 打印结果数组